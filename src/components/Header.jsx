import "./Header.css";
import headerHero from "../assets/header-hero.png";

export default function Header() {
  return (
    <section className="header__container">
      <div className="header__nav">
        <h1 className="header__mainTitle">Fonditas</h1>
        <nav className="header__menu">
          <p>Nosotros</p>
          <p>Fonditas</p>
          <p>Mapa</p>
          <p>Inscribírse</p>
        </nav>
      </div>
      <div className="header__contentContainer">
        <div className="header__content">
          <h6>De tu fonda favorita</h6>
          <h3>La comida que ya conoces al mejor precio</h3>
        </div> 
        <img src={headerHero} alt="Image en-tête" className="header__image" />
      </div>
    </section>
  );
}
