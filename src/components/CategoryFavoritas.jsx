import "./CategoryFavoritas.css";

export default function CategoryFavoritas(props) {
  const { logo, number, name, price } = props;
  return (
    <div className="individualFavoritas">
      <div className="individualFavoritas__logo">
        <div className="individualFavoritas__circle">
          <img src={logo} alt={`Logo de ${name}`} />
        </div>
        <div className="individualFavoritas__numberSquare">{number}</div>
      </div>
      <h4>{name}</h4>
      <p>Desde ${price}</p>
    </div>
  );
}