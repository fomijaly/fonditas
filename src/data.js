import pizzaLogo from "./assets/pizza-1.svg";
import sushiLogo from "./assets/sushi.svg";
import hamburgerLogo from "./assets/hamburger.svg";
import veggieLogo from "./assets/watermelon.svg";
import sopasLogo from "./assets/noodle.svg";
import postresLogo from "./assets/cake.svg";

import star from "./assets/star.svg";
import DonaLaura from "./assets/dish-dona-laura.jpeg";
import RosaCafe from "./assets/dish-rosa-cafe.jpeg";
import Cottidiene from "./assets/dish-cottidiene.jpeg";
import Querreque from "./assets/dish-querreque.jpeg";

import menuHotCaketerias from "./assets/menu-hot-caketerias.jpeg";
import menuDesayuno from "./assets/menu-desayunos.jpeg";
import otrosDesayunos from "./assets/menu-otros-desayunos.jpeg";
import pizzeria from "./assets/menu-pizzeria.jpeg";
import sushi from "./assets/menu-ensaladish.jpeg";
import pastish from "./assets/menu-pastish.jpeg";

const favoritasList = [
  {
    logo: pizzaLogo,
    number: 42,
    name: "Pizza",
    price: 60,
  },
  {
    logo: sushiLogo,
    number: 35,
    name: "Sushi",
    price: 50,
  },
  {
    logo: hamburgerLogo,
    number: 28,
    name: "Hamburguesas",
    price: 50,
  },
  {
    logo: veggieLogo,
    number: 23,
    name: "Veggie",
    price: 50,
  },
  {
    logo: sopasLogo,
    number: 35,
    name: "Sopas",
    price: 50,
  },
  {
    logo: postresLogo,
    number: 9,
    name: "Postres",
    price: 50,
  },
];
const fondasList = [
  {
    rating: 9.8,
    star: star,
    img: DonaLaura,
    title: "Doña Laura",
    duration: "20-30",
    type: "Fonditas",
  },
  {
    rating: 8.5,
    star: star,
    img: RosaCafe,
    title: "Rosa Cafe",
    duration: "~45",
    type: "Loncheria",
  },
  {
    rating: 7.3,
    star: star,
    img: Cottidiene,
    title: "Le cottidiene",
    duration: "15-20",
    type: "Sushi",
  },
  {
    rating: 8.0,
    star: star,
    img: Querreque,
    title: "Doña Laura",
    duration: "~50",
    type: "Veggies",
  },
];

const menus = [
  {
    name: "Hot caketerías",
    image: menuHotCaketerias,
    category: "Hot cakes",
    include: "dos toppics",
    time: "20-30",
    price: 70,
    review: 9.8,
  },
  {
    name: "Desayunos",
    image: menuDesayuno,
    category: "Continental",
    include: "huevo y tostadas",
    time: "15-20",
    price: 50,
    review: 9.8,
  },
  {
    name: "Otros desayunos",
    image: otrosDesayunos,
    category: "Hot cakes",
    include: "dos jugos y tocinito",
    time: "~20",
    price: 50,
    review: 9.8,
  },
  {
    name: "Pizzeria",
    image: pizzeria,
    category: "Pizza",
    include: "Chingo de pizzas",
    time: "~45",
    price: 70,
    review: 9.8,
  },
  {
    name: "Ensaladish",
    image: sushi,
    category: "Sushi",
    include: "Muchas frutas",
    time: "~45",
    price: 70,
    review: 9.8,
  },
  {
    name: "Pastish",
    image: pastish,
    category: "Pizza",
    include: "Una super duper pasta",
    time: "10-20",
    price: 50,
    review: 9.8,
  },
];

export {favoritasList, fondasList, menus};
